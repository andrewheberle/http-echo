package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type EchoHandle struct {
	title string
	tmpl  *template.Template
}

func NewEchoHandle(title, pattern string) (*EchoHandle, error) {
	tmpl, err := template.ParseGlob(pattern)
	if err != nil {
		return nil, err
	}

	return &EchoHandle{title, tmpl}, nil
}

func (echo EchoHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// gather data
	data := struct {
		Title   string
		Request map[string]string
		Headers http.Header
	}{
		Title: echo.title,
		Request: map[string]string{
			"Host":           r.Host,
			"Path":           r.URL.Path,
			"Query String":   r.URL.RawQuery,
			"Method":         r.Method,
			"Protocol":       r.Proto,
			"Remote Address": r.RemoteAddr,
		},
		Headers: r.Header,
	}

	// add CSP header
	w.Header().Set("Content-Security-Policy", "default-src 'self'; style-src 'self' cdnjs.cloudflare.com;")

	// execute template
	if err := echo.tmpl.Execute(w, data); err != nil {
		fmt.Fprintf(os.Stderr, "Echo Error: %s\n", err)
	}
}

func main() {
	// command line handling
	pflag.String("listen", ":8080", "listen address")
	pflag.String("title", "Echo Server", "application title")
	pflag.String("pattern", "*.tmpl", "template pattern")
	pflag.Parse()
	_ = viper.BindPFlags(pflag.CommandLine)

	// set up echo service
	echo, err := NewEchoHandle(viper.GetString("title"), viper.GetString("pattern"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}

	// set up router
	r := mux.NewRouter()
	r.PathPrefix("/").Handler(echo)

	srv := &http.Server{
		Addr:         viper.GetString("listen"),
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 5,
		Handler:      r,
	}

	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
