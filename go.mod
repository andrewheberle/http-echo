module gitlab.com/andrewheberle/http-echo

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.9.0
)
