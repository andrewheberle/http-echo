FROM golang:1.16 AS builder

COPY . /build

RUN cd /build && \
    CGO_ENABLED=0 go build -o /build/http-echo .

FROM gcr.io/distroless/static-debian10

COPY --from=builder /build/http-echo /app/http-echo
COPY *.tmpl /app/

EXPOSE 8080

WORKDIR /app

ENTRYPOINT [ "/app/http-echo" ]
